﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JuryDuty
{
    public partial class AlertStudent : System.Web.UI.Page
    {
        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNumber"] == null)
            {
                Response.Redirect("Default.aspx");
            }
        }
    }
}