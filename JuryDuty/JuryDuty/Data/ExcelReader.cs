﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;

namespace JuryDuty.Data
{
    public class ExcelReader
    {
        Dictionary<String, String> props = new Dictionary<string, string>();
        String connectionString;

        public ExcelReader(String readLocation)
        {
            props["Provider"] = "Microsoft.ACE.OLEDB.12.0";
            props["Extended Properties"] = "Excel 12.0";
            props["Data Source"] = readLocation;

            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<String, String> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }
            connectionString = sb.ToString();
        }

        public DataSet ReadExcelFile()
        {
            DataSet ds = new DataSet();

            try
            {
                using (OleDbConnection conn = new OleDbConnection(connectionString))
                {
                    conn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = conn;

                    DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    foreach (DataRow dr in dtSheet.Rows)
                    {
                        String sheetName = dr["TABLE_NAME"].ToString();

                        if (!sheetName.EndsWith("$"))
                            continue;

                        cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                        DataTable dt = new DataTable();
                        dt.TableName = sheetName;

                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(dt);
                        ds.Tables.Add(dt);
                    }

                    cmd = null;
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e.Message);
                throw e;
            }
            
            return ds;
        }

        public void WriteToPassKeyExcelFile(Dictionary<String, String> data)
        {
            try
            {
                using (OleDbConnection conn = new OleDbConnection(connectionString))
                {
                    conn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = conn;

                    cmd.CommandText = "CREATE TABLE [StudentsPasskey] (SNumber VARCHAR, Passkey VARCHAR)";
                    cmd.ExecuteNonQuery();

                    foreach (KeyValuePair<String, String> passkey in data)
                    {
                        cmd.CommandText = String.Format("INSERT INTO StudentsPasskey (SNumber, Passkey) values('{0}','{1}')", passkey.Key, passkey.Value);
                        cmd.ExecuteNonQuery();
                    }
                    cmd = null;
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e.Message);
                throw e;
            }
        }
    }
}