﻿using JuryDuty.database;
using System.Diagnostics;
using JuryDuty.scheduler.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JuryDuty
{
    public partial class signedUp : System.Web.UI.Page
    {
        private String appPath;
        private String dbPath;
        private Student stu = new Student();

        protected void return_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentView.aspx");
        }
        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNumber"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            this.appPath = Request.PhysicalApplicationPath;
            this.dbPath = @"\database\";
            SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", appPath, dbPath), false);
            DataTable dt = db.GetDataTable(String.Format("SELECT firstName, lastName, classification, accompanist_ID, isVerified, isSignedUp FROM Student WHERE sNumber='{0}'", Session["sNumber"]));
            
            foreach (DataRow row in dt.Rows)
            {
                stu.FirstName = row["firstName"].ToString();
                stu.LastName = row["lastName"].ToString();
                
            }

            DataTable stdt = db.GetDataTable("SELECT startTime, endTime, roomNumber FROM Timeblock WHERE Timeblock.studentFirstName='" + stu.FirstName + "' AND Timeblock.studentLastName='" + stu.LastName + "'");

            
            String test = "";

            foreach (DataRow row in stdt.Rows)
            {
                test += " " + row["startTime"].ToString() + " to " + row["endTime"] + " in Room " + row["roomNumber"];
            }

            

            fullName.InnerText = stu.FirstName + " " + stu.LastName;
            Times.InnerText = "Congrats! Your Jury Time is " + test;
        }

      

    }
}