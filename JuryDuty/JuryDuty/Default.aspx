﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JuryDuty.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Fine Arts Finals </title>
    <link rel="stylesheet" type="text/css" href="css/juryDuty.css"/>
</head>
<body>
    <div class="layer">
		<div id="outsideBorder">
		    <h1>Fine and Performing Arts</h1>
            <div id="mainBorder">
		        <h3> Please login below to setup your final exam schedule.</h3>
                <form id="form1" runat="server">
                    <table border="0">               
                        <tr>                      
                            <td colspan="2"><asp:TextBox runat="server" placeholder="Username" class="textBox" id="username"></asp:TextBox></td>
                        </tr>
                        <tr>                       
                            <td colspan="2" ><asp:TextBox runat="server" placeholder="Password" class="textBox" id="password" TextMode="Password"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:Button runat="server" class="button" type="submit" Text="Login" OnClick="loginUser" /> </td>                       
                        </tr>               
                    </table>
                    <p runat="server" id="errorText"></p>
                </form>
            </div>
        </div>
    </div>    
</body>
</html>
