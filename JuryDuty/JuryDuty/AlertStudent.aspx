﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlertStudent.aspx.cs" Inherits="JuryDuty.AlertStudent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Important</h1>
        <p> Email the head of your department correcting the verification,
            include your first and last name, your accompanist, and your classification(example woodwind) in your email. 
            Thank you!
        </p>
        <div class="logout">          
         <asp:Button runat="server" ID="logout" OnClick="logout_Click" text="Sign Out" />
    </div>
    <div>
    
    </div>
    </form>
</body>
</html>
