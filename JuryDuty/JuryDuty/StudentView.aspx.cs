﻿using JuryDuty.database;
using System.Diagnostics;
using JuryDuty.scheduler.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JuryDuty
{
    public partial class StudentView : System.Web.UI.Page
    {
        private String appPath;
        private String dbPath;
        private Student stu = new Student();
        private SQLiteDatabase db;
        private DataTable tdt;

        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
                if (Session["sNumber"] == null)
                {
                    Response.Redirect("Default.aspx");
                }
                this.appPath = Request.PhysicalApplicationPath;
                this.dbPath = @"\database\";
                db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", appPath, dbPath), false);
                DataTable dt = db.GetDataTable(String.Format("SELECT firstName, lastName, classification, accompanist_ID, isVerified, isSignedUp FROM Student WHERE sNumber='{0}'", Session["sNumber"]));
                Int32 accomp_id = Int32.Parse(dt.Rows[0]["accompanist_ID"].ToString());
                DataTable adt = db.GetDataTable("SELECT firstName, lastName FROM Accompanist WHERE Accompanist.accompanist_ID=" + accomp_id);

                String verified = dt.Rows[0]["isVerified"].ToString();
                String isSignedUp = dt.Rows[0]["isSignedUp"].ToString();
                String firstName = "";
                String lastName = "";
                String classifications = "";
                String acompFirstName = "";
                String acompLastName = "";

                foreach (DataRow row in dt.Rows)
                {
                    firstName = row["firstName"].ToString();
                    lastName = row["lastName"].ToString();
                    classifications = row["classification"].ToString();

                }
                foreach (DataRow row in adt.Rows)
                {
                    acompFirstName = row["firstName"].ToString();
                    acompLastName = row["lastName"].ToString();

                }

                if (verified == "0")
                {
                    Response.Redirect("StudentVerfification.aspx");
                }
                stu.FirstName = firstName;
                stu.LastName = lastName;
                stu.Classification = classifications;
                stu.signedUp = isSignedUp;
                setTDT();

                DataTable stdt = db.GetDataTable("SELECT startTime, endTime, roomNumber FROM Timeblock WHERE Timeblock.studentFirstName='" + stu.FirstName + "' AND Timeblock.studentLastName='" + stu.LastName + "'");

                String signedUpTime = "";

                foreach (DataRow row in stdt.Rows)
                {
                    signedUpTime += " " + row["startTime"].ToString() + " to " + row["endTime"] + " in Room " + row["roomNumber"];
                }

                rebindGridView();

                fullName.InnerText = firstName + " " + lastName;
                if(stu.signedUp == "0"){
                    Times.InnerText = "Sign up!";
                }
                else
                {
                    Times.InnerText = "You have signed up for: " + signedUpTime + ".";
                    nextLine.InnerText = " If you click sign up again, your time will switch";
                }
        
                
                
                //classification.InnerText = classifications;
                //accompanist.InnerText = " " + acompFirstame + " " + acompLastName;
                     
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Signed")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                int index = Convert.ToInt32(e.CommandArgument);

                // Retrieve the row that contains the button 
                // from the Rows collection.
                GridViewRow row = GridView1.Rows[index];
                string startTime = row.Cells[0].Text.ToString();
                signUp();

                if (stu.signedUp == "0")
                {
                    fillStartTime(startTime);
                    setTDT();
                    rebindGridView();
                }else
                {
                    deleteStudentName();
                    fillStartTime(startTime);
                    setTDT();
                    rebindGridView();

                }
                Response.Redirect("signedUp.aspx");
                    
                
                
                //Times.InnerText = 

                // Add code here to add the item to the shopping cart.
            }
        }

        private void fillStartTime(string startTime)
        {

            Dictionary<String, String> updateData = new Dictionary<string, string>();
            updateData["studentFirstName"] = stu.FirstName;
            updateData["studentLastName"] = stu.LastName;
            db.Update("Timeblock", updateData, "classification='" + stu.Classification + "' And startTime='" + startTime + "'");



        }

        private void deleteStudentName()
        {

            Dictionary<String, String> updateData = new Dictionary<string, string>();
            updateData["studentFirstName"] = "";
            updateData["studentLastName"] = "";
            db.Update("Timeblock", updateData, "studentFirstName='" + stu.FirstName + "' And studentLastName='" + stu.LastName + "'");



        }

        private void signUp()
        {
            String one = "1";
            Dictionary<String, String> updateData = new Dictionary<string, string>();
            updateData["isSignedUp"] = one;
            db.Update("Student", updateData, "firstName='" + stu.FirstName + "' And lastName='" + stu.LastName + "'");
            stu.signedUp = "1";
        }

        private void setTDT(){

            tdt = db.GetDataTable("SELECT startTime, endTime, roomNumber FROM Timeblock WHERE Timeblock.classification='" + stu.Classification + "' AND Timeblock.studentFirstName IS NULL OR Timeblock.studentFirstName=''");

        }

        private void rebindGridView()
        {

            if (!Page.IsPostBack)
            {
                
                GridView1.DataSource = tdt;
                GridView1.DataBind();
                                
            }

            
        }
    }

   
}