﻿using JuryDuty.database;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JuryDuty
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void loginUser(object sender, EventArgs e)
        {
            try
            {
                String appPath = Request.PhysicalApplicationPath;
                String dbPath = @"\database\";
                SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", appPath, dbPath), true);
                String uName = db.ExecuteScalar(String.Format("SELECT username FROM Admin WHERE passkey='{0}'", password.Text));
                String sNumber = db.ExecuteScalar(String.Format("SELECT sNumber FROM Student WHERE passkey='{0}'", password.Text));
                if (uName.ToLower().Equals(username.Text.ToLower()) && !uName.Equals(""))
                {
                    Session["username"] = uName;
                    Response.Redirect("~/admin.views/AdminView.aspx");
                }
                else if (sNumber.ToLower().Equals(username.Text.ToLower()) && !sNumber.Equals(""))
                {
                    Session["sNumber"] = sNumber;
                    Response.Redirect("~/StudentView.aspx");
                }
                else
                {
                    errorText.InnerText = "Invalid credentials. Please re-enter information";
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}