﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentView.aspx.cs" Inherits="JuryDuty.StudentView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Jury Duty</title>
    <link type="text/css" rel="stylesheet" href="~/css/studentView.css"/>
</head>
<body>
    <div class="layer"> 
        <div id ="outsideBorder">
    <h1>Welcome <span runat="server" id="fullName"></span>!</h1>
    <h2> <span runat="server" id="Times"></span></h2>
    <h3> <span runat="server" id="nextLine"></span></h3>
     
    <form id="form1" runat="server">
        <div class="logout">          
         <asp:Button runat="server" ID="logout" OnClick="logout_Click" text="Sign Out" />
    </div>
      <asp:GridView ID="GridView1" HeaderStyle-BackColor="#006747" HeaderStyle-ForeColor="White"
        runat="server" AutoGenerateColumns="false" onrowcommand="GridView1_RowCommand" CellPadding ="5" HeaderStyle-Font-Size ="Larger">
        <Columns>
            <asp:BoundField DataField="startTime" HeaderText="Start Time" ItemStyle-Width="100" ItemStyle-HorizontalAlign ="Center" ItemStyle-Font-Bold ="true" ItemStyle-Font-Size="Large"/>
            <asp:BoundField DataField="endTime" HeaderText="End Time" ItemStyle-Width="100" ItemStyle-HorizontalAlign ="Center" ItemStyle-Font-Bold ="true" ItemStyle-Font-Size="Large"/>
            <asp:BoundField DataField="roomNumber" HeaderText="Room Number" ItemStyle-Width="140" ItemStyle-HorizontalAlign ="Center" ItemStyle-Font-Bold ="true" ItemStyle-Font-Size="Large"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="Signed" runat="server"
                        CommandName="Signed" 
                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                           Text="Sign Up"
                            
                        
                        />
                </ItemTemplate>
            </asp:TemplateField>
         </Columns>
          
    </asp:GridView>
        <br />

   
    </form>
        </div>
        </div>
</body>
</html>
