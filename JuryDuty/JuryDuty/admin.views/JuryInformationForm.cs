﻿using JuryDuty.database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JuryDuty.admin.views
{
    public class JuryInformationForm : System.Web.UI.ITemplate
    {
        public String date;
        private String appPath;
        private String dbPath;
        private TextBox timeBlockName;
        private TextBox classification;
        private TextBox roomNumber;
        private DropDownList timeBlockInterval;
        private TextBox startTime;
        private DropDownList startTimeAM_PM;
        private TextBox endTime;
        private DropDownList endTimeAM_PM;
        private ListBox timeblocks;
        private Button addTimeblockbtn;
        private Label updateInfolbl;

        public JuryInformationForm(String date, String dbAppPath, String dbPath)
        {
            this.date = date;
            this.appPath = dbAppPath;
            this.dbPath = dbPath;
            
            timeBlockName = new TextBox();
            timeBlockName.ID = "timeBlockName";
            
            classification = new TextBox();
            classification.ID = "classification";
            
            roomNumber = new TextBox();
            roomNumber.ID = "roomNumber";
            
            timeBlockInterval = new DropDownList();
            timeBlockInterval.Items.Add(new ListItem("10"));
            timeBlockInterval.Items.Add(new ListItem("15"));
            timeBlockInterval.Items.Add(new ListItem("20"));
            
            startTime = new TextBox();
            startTime.ID = "startTime";
            startTimeAM_PM = new DropDownList();
            startTimeAM_PM.Items.Add(new ListItem("AM"));
            startTimeAM_PM.Items.Add(new ListItem("PM"));
            
            endTime = new TextBox();
            endTime.ID = "endTime";
            endTimeAM_PM = new DropDownList();
            endTimeAM_PM.Items.Add(new ListItem("AM"));
            endTimeAM_PM.Items.Add(new ListItem("PM"));
            
            this.timeblocks = new ListBox();
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            PlaceHolder ph = new PlaceHolder();
            Label timeBlockNamelbl = new Label();
            timeBlockNamelbl.Text = "Timeblock Name";
            Label classificationlbl = new Label();
            classificationlbl.Text = "Classification: ";
            Label roomNumberlbl = new Label();
            roomNumberlbl.Text = "Room Number: ";
            Label timeBlockIntervallbl = new Label();
            timeBlockIntervallbl.Text = "Individual Session Time (min): ";
            Label startTimelbl = new Label();
            startTimelbl.Text = "Start Time: ";
            Label endTimelbl = new Label();
            endTimelbl.Text = "End Time: ";

            ph.Controls.Add(timeBlockNamelbl);
            ph.Controls.Add(timeBlockName);
            ph.Controls.Add(classificationlbl);
            ph.Controls.Add(classification);
            ph.Controls.Add(roomNumberlbl);
            ph.Controls.Add(roomNumber);
            ph.Controls.Add(timeBlockIntervallbl);
            ph.Controls.Add(timeBlockInterval);
            ph.Controls.Add(startTimelbl);
            ph.Controls.Add(startTime);
            ph.Controls.Add(startTimeAM_PM);
            ph.Controls.Add(endTimelbl);
            ph.Controls.Add(endTime);
            ph.Controls.Add(endTimeAM_PM);
            populateListBox();
            ph.Controls.Add(timeblocks);
            
            this.addTimeblockbtn = new Button();
            this.addTimeblockbtn.ID = "addTimeblockBtn";
            this.addTimeblockbtn.Text = "Add Timeblock";
            this.addTimeblockbtn.CausesValidation = true;
            this.addTimeblockbtn.Click += new EventHandler(addTimeblockbtn_Click);
            UpdatePanel uPanel = new UpdatePanel();
            uPanel.ChildrenAsTriggers = true;
            uPanel.ContentTemplateContainer.Controls.Add(addTimeblockbtn);
            AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
            trigger.ControlID = "addTimeblockBtn";
            trigger.EventName = "Click";
            uPanel.Triggers.Add(trigger);
            ph.Controls.Add(uPanel);
            this.updateInfolbl = new Label();
            this.updateInfolbl.Visible = false;
            this.updateInfolbl.ID = "updateInfolbl";
            ph.Controls.Add(updateInfolbl);
            
            container.Controls.Add(ph);
        }

        private void addTimeblockbtn_Click(object sender, EventArgs e)
        {
            SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), false);
            String timeblockID = db.ExecuteScalar("SELECT timeblockID FROM Jury WHERE date = \'" + this.date + "\';");
            Dictionary<String, String> data = new Dictionary<String, String>();
            char[] delimitters = { ':', '/' };
            String[] dateSplit = this.date.Split(delimitters);
            String[] startTimeSplit = this.startTime.Text.Split(delimitters);
            String[] endTimeSplit = this.endTime.Text.Split(delimitters);
            DateTime startDateTime = new DateTime(Int32.Parse(dateSplit[2]), Int32.Parse(dateSplit[0]), Int32.Parse(dateSplit[1]), Int32.Parse(startTimeSplit[0]), Int32.Parse(startTimeSplit[1]), 0);
            DateTime endDateTime = new DateTime(Int32.Parse(dateSplit[2]), Int32.Parse(dateSplit[0]), Int32.Parse(dateSplit[1]), Int32.Parse(endTimeSplit[0]), Int32.Parse(endTimeSplit[1]), 0);
            TimeSpan ts = endDateTime - startDateTime;
            int timeDiff = (ts.Hours * 60) + ts.Minutes;
            while (timeDiff != 0)
            {
                data["timeblockID"] = timeblockID;
                data["timeblockName"] = this.timeBlockName.Text;
                data["startTime"] = startDateTime.Hour + ":" + ((startDateTime.Minute == 0) ? "00" : startDateTime.Minute.ToString());
                Int32 addMins = Int32.Parse(this.timeBlockInterval.SelectedValue);
                startDateTime = startDateTime.AddMinutes(addMins);
                data["endTime"] = startDateTime.Hour + ":" + ((startDateTime.Minute == 0) ? "00" : startDateTime.Minute.ToString());
                data["classification"] = this.classification.Text;
                data["roomNumber"] = this.roomNumber.Text;
                db.Insert("Timeblock", data);
                ts = endDateTime - startDateTime;
                timeDiff = (ts.Hours * 60) + ts.Minutes;
            }
            populateListBox();
            resetControls();
            updateInfolbl.Visible = true;
            updateInfolbl.Text = "Changes made Successfully!";
        }


        private void populateListBox()
        {
            this.timeblocks.Items.Clear();
            SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), true);
            String timeblockID = db.ExecuteScalar("SELECT timeblockID FROM Jury WHERE date = \'" + this.date + "\';");
            DataTable results = db.GetDataTable("SELECT DISTINCT timeblockName FROM Timeblock WHERE timeblockID = " + timeblockID + ";");
            foreach (DataRow row in results.Rows)
            {
                this.timeblocks.Items.Add(new ListItem(row["timeblockName"].ToString()));
            }
        }

        private void resetControls()
        {
            this.timeBlockName.Text = "";
            this.classification.Text = "";
            this.roomNumber.Text = "";
            this.timeBlockInterval.SelectedIndex = 0;
            this.startTime.Text = "";
            this.startTimeAM_PM.SelectedIndex = 0;
            this.endTime.Text = "";
            this.endTimeAM_PM.SelectedIndex = 0;
        }
    }
}