﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminView.aspx.cs" Inherits="JuryDuty.AdminView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Jury Duty</title>
    <link type="text/css" rel="stylesheet" href="~/css/adminNew.css"/>
    <script type="text/javascript">
        function closeSetup() {
            $find('modalWindow').hide();
        }
        function openSetup() {
            $find('modalWindow').show();
        }
        function validateInfo() {
            alert("There were errors");
        }
    </script>
</head>
<body>
    <div class="layer">
        <form id="form1" runat="server">
        <div id="navBar">
            <h1>Welcome <span runat="server" id="fullName"></span>!
            <asp:Button runat="server" ID="logoutBTN" OnClick="logout_Click" class="navItem" text="Sign Out" />
            </h1>
        </div><br />
        <div id="content">
            <asp:ScriptManager runat="server" ID="asm"/>
            <asp:Panel ID="JurySetupModal" runat="server" ScrollBars="Vertical">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Panel runat="server">
                            <div class="calendarContainer">
                                <div class="dateContainer">
                                    Start Date:
                                    <asp:TextBox ID="StartDateBox" runat="server" />
                                </div>
                                <div class="iconContainer">
                                    <asp:Image ID="calIconStart" runat="server" ImageUrl="~/images/calendarIcon.png" />
                                </div>
                                <div class="dateContainer">
                                    End Date:
                                    <asp:TextBox ID="EndDateBox" runat="server" />
                                </div>
                                <div class="iconContainer">
                                    <asp:Image ID="calIconEnd" runat="server" ImageUrl="~/images/calendarIcon.png" />
                                </div>
                                <asp:Button ID="SetupDates" runat="server" Text="Setup Dates" OnClick="SetupDates_Click" />
                            </div>
                            <div>
                                <ajaxToolkit:TabContainer ID="DateTabContainer" runat="server"></ajaxToolkit:TabContainer>
                            </div>
                            <ajaxToolkit:CalendarExtender ID="ceStart" runat="server" TargetControlID="StartDateBox" PopupButtonID="calIconStart"/>
                            <ajaxToolkit:CalendarExtender ID="ceEnd" runat="server" TargetControlID="EndDateBox" PopupButtonID="calIconEnd" />
                            <span runat="server" id="error"></span>
                            <asp:Button ID="Validate" runat="server" Text="Validate" OnClick="Validate_Click" />
                            <asp:Button ID="Done" runat="server" Text="Done" OnClientClick='closeSetup();' />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <div id="contentLeft">
                <asp:FileUpload runat="server" id="studentInfoUpload" />
                <asp:Button runat="server" CssClass="button" OnClick="enterStudentInfo_Click" Text="Enter Student Information" /><br />
                <asp:Button runat="server" CssClass="button" Text="Clear Student Data" OnClick="clearStudentTable"
                    OnClientClick="return confirm('This will remove all Student data. Continue?');" /><br />
                <asp:Button runat="server" CssClass="button" ID="setupJurySchedule" Text="Setup Jury Schedule"  />
                <ajaxToolkit:ModalPopupExtender ID="mpe" runat="server" PopupControlID="JurySetupModal" BehaviorID="modalWindow" TargetControlID="setupJurySchedule" OkControlID="Done"/>
            </div>
            <div id="contentRight">
                Right content
            </div>
        </div>
        </form>
    </div>
</body>
</html>
