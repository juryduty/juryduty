﻿using JuryDuty.admin.views;
using JuryDuty.Data;
using JuryDuty.database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace JuryDuty
{
    public partial class AdminView : System.Web.UI.Page
    {
        private String appPath;
        private String dbPath;
        private JuryInformationForm contentTemplate;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.appPath = Request.PhysicalApplicationPath;
            this.dbPath = @"\database\";
            if(!Page.IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", appPath, dbPath), true);
                DataTable dt = db.GetDataTable(String.Format("SELECT firstName, lastName FROM Admin WHERE username='{0}'", Session["username"]));
                DataTable juryDates = db.GetDataTable("SELECT * FROM Jury ORDER BY date");
                String firstName = "";
                String lastName = "";
                foreach (DataRow row in dt.Rows)
                {
                    firstName = row["firstName"].ToString();
                    lastName = row["lastName"].ToString();
                }
                fullName.InnerText = firstName + " " + lastName;
                StartDateBox.Text = juryDates.Rows[0]["date"].ToString();
                EndDateBox.Text = juryDates.Rows[juryDates.Rows.Count - 1]["date"].ToString();
            }
            updateJuryDates();
        }


        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("../Default.aspx");
        }

        protected void enterStudentInfo_Click(object sender, EventArgs e)
        {
            String saveLocation = @"Data\";
            String savePath = this.appPath + saveLocation;
            if (studentInfoUpload.HasFile)
            {
                String fileName = Server.HtmlEncode(studentInfoUpload.FileName);
                String extension = System.IO.Path.GetExtension(fileName);

                if ((extension == ".xls") || (extension == ".xlsx"))
                {
                    savePath += fileName;
                    studentInfoUpload.SaveAs(savePath);

                    ExcelReader er = new ExcelReader(savePath);
                    DataSet ds = er.ReadExcelFile();
                    addAccompanistsToDb(ds);
                    addStudentsToDb(ds, er);
                }
                else
                {
                    //TODO: Notify user why their file was not uploaded.
                }
            }
        }

        protected void clearStudentTable(object sender, EventArgs e)
        {
            SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), false);
            db.ClearTable("Student");
        }


        private void addAccompanistsToDb(DataSet ds)
        {
            try
            {
                DataTable accompanistDT = ds.Tables["Accompanists$"];
                SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), false);
                foreach (DataRow row in accompanistDT.Rows)
                {
                    Dictionary<String, String> data = new Dictionary<string,string>();
                    data.Add("firstName", row["First Name"].ToString());
                    data.Add("lastName", row["Last Name"].ToString());
                    db.Insert("Accompanist", data);
                }
            }
            catch (Exception)
            {

            }
        }

        private void addStudentsToDb(DataSet ds, ExcelReader er)
        {
            try
            {
                DataTable studentDT = ds.Tables["Students$"];
                String appPath = Request.PhysicalApplicationPath;
                String dbPath = @"\database\";
                Dictionary<String, String> passkeys = new Dictionary<String, String>();
                SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", appPath, dbPath), false);
                foreach (DataRow row in studentDT.Rows)
                {
                    Dictionary<String, String> data = new Dictionary<string, string>();
                   
                    String sNumber = row["S Number"].ToString();
                    data.Add("sNumber", sNumber);
                    data.Add("firstName", row["First Name"].ToString());
                    data.Add("lastName", row["Last Name"].ToString());
                    data.Add("classification", row["Classification"].ToString());
                    String[] accompanistName = row["Accompanist Name"].ToString().Split(null);
                    String accompanistFirstName = accompanistName[0];
                    String accompanistLastName = accompanistName[1];
                    String accompanistID = db.ExecuteScalar("SELECT accompanist_ID FROM Accompanist WHERE Accompanist.firstName='" + accompanistFirstName +
                        "' and Accompanist.lastName='" + accompanistLastName + "'");
                    data.Add("accompanist_ID", accompanistID);
                    String passKey = generatePassKey();
                    data.Add("passkey", passKey);
                    data.Add("isVerified", "0");
                    data.Add("isSignedUp", "0");
                    passkeys[sNumber] = passKey;
                    db.Insert("Student", data);
                }
                er.WriteToPassKeyExcelFile(passkeys);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e.Message);
            }
        }

        private String generatePassKey()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, 5)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        protected void Validate_Click(object sender, EventArgs e)
        {
            Debug.Print("hello");
        }

        protected void SetupDates_Click(object sender, EventArgs e)
        {
            char[] delimitters = {'/'};
            String[] startDate = StartDateBox.Text.Split(delimitters);
            String[] endDate = EndDateBox.Text.Split(delimitters);
            DateTime startDateTime = new DateTime(Int32.Parse(startDate[2]), Int32.Parse(startDate[0]), Int32.Parse(startDate[1]));
            DateTime endDateTime = new DateTime(Int32.Parse(endDate[2]), Int32.Parse(endDate[0]), Int32.Parse(endDate[1]));
            if (startDateTime < endDateTime)
            {
                deleteUnusedTabs(startDateTime, endDateTime);
                error.InnerText = "";
                SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), false);
                TimeSpan ts = endDateTime - startDateTime;
                int numOfDays = ts.Days;
                Random rand = new Random();
                for(int i = 0; i <= numOfDays; i++)
                {
                    Dictionary<String, String> data = new Dictionary<string, string>();
                    data.Add("date", startDateTime.ToString("MM/dd/yyyy"));
                    int results = db.GetDataTable(String.Format("SELECT * FROM Jury WHERE date = \"{0}\"", data["date"])).Rows.Count;
                    if(results == 0)
                    {
                        data.Add("timeblockID", rand.Next(1000, 10000).ToString());
                        db.Insert("Jury", data);
                    }
                    startDateTime = startDateTime.AddDays(1);
                }
                updateJuryDates();
            }
            else
            {
                error.InnerText = "Invalid Dates: End date occurs either on or before the Start Date";
            }
        }

        private void deleteUnusedTabs(DateTime startTime, DateTime endTime)
        {
            SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), false);
            DataTable dt = db.GetDataTable("SELECT date FROM Jury ORDER BY date");
            foreach(DataRow row in dt.Rows)
            { 
                char[] delimitters = {'/'};
                String[] date = row["date"].ToString().Split(delimitters);
                DateTime dateTime = new DateTime(Int32.Parse(date[2]), Int32.Parse(date[0]), Int32.Parse(date[1]));
                if((dateTime < startTime) || (dateTime > endTime))
                {
                    String where = String.Format("date = \"{0}\"", row["date"].ToString());
                    db.Delete("Jury", where);
                }
            }
        }

        private void updateJuryDates()
        {
            DateTabContainer.Tabs.Clear();
            SQLiteDatabase db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", this.appPath, this.dbPath), true);
            DataTable juryDates = db.GetDataTable("SELECT * FROM Jury ORDER BY date");
            if (juryDates.Rows.Count != 0)
            {
                foreach (DataRow row in juryDates.Rows)
                {
                    AjaxControlToolkit.TabPanel tab = new AjaxControlToolkit.TabPanel();
                    tab.HeaderText = row["date"].ToString();
                    this.contentTemplate = new JuryInformationForm(tab.HeaderText, this.appPath, this.dbPath); ;
                    tab.ContentTemplate = this.contentTemplate;
                    DateTabContainer.Tabs.Add(tab);
                }
            }
        }
    }
}