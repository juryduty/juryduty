﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Linq;
using System.Web;
using System.Diagnostics;

namespace JuryDuty.database
{
    public class SQLiteDatabase
    {
        String dbConnection;

        /// <summary>
        /// Creates a new database connection.
        /// </summary>
        /// <param name="dbName">database file name</param>
        /// <param name="isReadOnly">read only flag</param>
        public SQLiteDatabase(String dbName, bool isReadOnly)
        {
            dbConnection = String.Format("Data Source={0};Version=3;Read Only={1}", dbName, isReadOnly);
            SQLiteConnection conn = new SQLiteConnection(dbConnection, true);
            try
            {
                conn.Open();
                Debug.WriteLine("Successful Connection");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Connection was not established: " + e.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Returns results based on the given query.
        /// </summary>
        /// <param name="sql">SQL string</param>
        /// <returns>Query results</returns>
        public DataTable GetDataTable(String sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SQLiteConnection conn = new SQLiteConnection(dbConnection);
                conn.Open();
                SQLiteCommand sqlCommand = new SQLiteCommand(conn);
                sqlCommand.CommandText = sql;
                SQLiteDataReader reader = sqlCommand.ExecuteReader();
                dt.Load(reader);
                reader.Close();
                conn.Close();
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Could not return Data Table.\n{0}", e.Message));
            }
            return dt;
        }

        /// <summary>
        /// Run any SQL statement against the database.
        /// </summary>
        /// <param name="sql">SQL string</param>
        /// <returns>Number of rows updated</returns>
        public int ExecuteNonQuery(String sql)
        {
            SQLiteConnection conn = new SQLiteConnection(dbConnection);
            conn.Open();
            SQLiteCommand sqlCommand = new SQLiteCommand(conn);
            sqlCommand.CommandText = sql;
            int rowsUpdated = sqlCommand.ExecuteNonQuery();
            conn.Close();
            return rowsUpdated;
        }

        /// <summary>
        /// Returns one value from the database with the given query.
        /// </summary>
        /// <param name="sql">SQL string</param>
        /// <returns>Result of query (Can be null)</returns>
        public String ExecuteScalar(String sql)
        {
            SQLiteConnection conn = new SQLiteConnection(dbConnection);
            conn.Open();
            SQLiteCommand sqlCommand = new SQLiteCommand(conn);
            sqlCommand.CommandText = sql;
            object value = sqlCommand.ExecuteScalar();
            conn.Close();
            if (value != null)
            {
                return value.ToString();
            }
            return "";
        }

        /// <summary>
        /// Updates database file.
        /// </summary>
        /// <param name="tableName">Table to be effected</param>
        /// <param name="data">where to update <Column Name, New value></param>
        /// <param name="where">condition</param>
        /// <returns></returns>
        public bool Update(String tableName, Dictionary<String, String> data, String where)
        {
            String vals = "";
            Boolean returnCode = true;
            if (data.Count >= 1)
            {
                foreach(KeyValuePair<String, String> val in data)
                {
                    vals += String.Format(" {0} = '{1}',", val.Key.ToString(), val.Value.ToString());
                }
                vals = vals.Substring(0, vals.Length - 1);
            }
            try
            {
                this.ExecuteNonQuery(String.Format("update {0} set {1} where {2};", tableName, vals, where));
            }
            catch
            {
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        /// Deletes record in the database
        /// </summary>
        /// <param name="tableName">tableName to be effected</param>
        /// <param name="where">condition</param>
        /// <returns>True for success, False for failure</returns>
        public bool Delete(String tableName, String where)
        {
            Boolean returnCode = true;
            try
            {
                this.ExecuteNonQuery(String.Format("delete from {0} where {1};", tableName, where));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                returnCode = false;
            }
            return returnCode;
        }


        /// <summary>
        /// Inserts records into database
        /// </summary>
        /// <param name="tableName">tableName to be effected</param>
        /// <param name="data">where to insert <Column Name, Value></param>
        /// <returns>True for success, False for Failure</returns>
        public bool Insert(String tableName, Dictionary<String, String> data)
        {
            String columns = "";
            String values = "";
            Boolean returnCode = true;
            foreach(KeyValuePair<String, String> val in data)
            {
                columns += String.Format(" {0},", val.Key.ToString());
                values += String.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);
            try
            {
                this.ExecuteNonQuery(String.Format("insert into {0}({1}) values({2});", tableName, columns, values));
            }
            catch(Exception e)
            {
                Debug.WriteLine("SQL Entry Error: " + e.Message);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        /// Clears all tables in the database
        /// </summary>
        /// <returns>True for success, False for failure</returns>
        public bool ClearDB()
        {
            DataTable tables;
            try
            {
                tables = this.GetDataTable("select NAME from SQLITE_MASTER where type='table' order by NAME;");
                foreach (DataRow table in tables.Rows)
                {
                    this.ClearTable(table["NAME"].ToString());
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Clears a single tableName in the database
        /// </summary>
        /// <param name="tableName">tableName to be effected</param>
        /// <returns>True for success, False for failure</returns>
        public bool ClearTable(String tableName)
        {
            try
            {
                this.ExecuteNonQuery(String.Format("delete from {0};", tableName));
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}