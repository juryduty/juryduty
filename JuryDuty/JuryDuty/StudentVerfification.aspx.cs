﻿using JuryDuty.database;
using JuryDuty.scheduler.entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JuryDuty
{
    public partial class StudentVerfification : System.Web.UI.Page
    {
        private String appPath;
        private String dbPath;
        private Student stu = new Student();
        DataTable dt;
        SQLiteDatabase db;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["sNumber"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            this.appPath = Request.PhysicalApplicationPath;
            this.dbPath = @"\database\";
            this.db = new SQLiteDatabase(String.Format("{0}{1}jurydutydb.db", appPath, dbPath), false);
            this.dt = db.GetDataTable(String.Format("SELECT firstName, lastName, classification, accompanist_ID, isVerified FROM Student WHERE sNumber='{0}'", Session["sNumber"]));
            Int32 accomp_id = 1;
            DataTable adt = db.GetDataTable("SELECT firstName, lastName FROM Accompanist WHERE Accompanist.accompanist_ID=" + accomp_id);

            Int32 verified = Int32.Parse(dt.Rows[0]["isVerified"].ToString());
            String firstName = "";
            String lastName = "";
            String classifications = "";
            String acompFirstName = "";
            String acompLastName = "";

            foreach (DataRow row in dt.Rows)
            {
                firstName = row["firstName"].ToString();
                lastName = row["lastName"].ToString();
                classifications = row["classification"].ToString();

            }
            foreach (DataRow row in adt.Rows)
            {
                acompFirstName = row["firstName"].ToString();
                acompLastName = row["lastName"].ToString();

            }
            stu.FirstName = firstName;
            stu.LastName = lastName;
            fullName.InnerText = firstName + " " + lastName;
            classification.InnerText = classifications;
            accompanist.InnerText = " " + acompFirstName + " " + acompLastName;
        }

        protected void yes_Click(object sender, EventArgs e)
        {
            isVerified(); 
            Response.Redirect("~/StudentView.aspx");
            
            
        }
        protected void no_Click(object sender, EventArgs e)
        {
            
            Response.Redirect("AlertStudent.aspx");
        }
        private void isVerified()
        {
            String one = "1";
            Dictionary<String, String> updateData = new Dictionary<string, string>();
            updateData["isVerified"] = one;
            db.Update("Student", updateData, "firstName='" + stu.FirstName + "' And lastName='" + stu.LastName +"'");



        }
    }
}