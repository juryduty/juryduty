﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JuryDuty.scheduler.entities
{
    public class Student
    {
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Classification { get; set; }
        public String AccompanistName { get; set; }
        public int Verification { get; set;}
        public String signedUp{ get; set;}
        
        public Student(int scheduleSlots, String firstName, String lastName,
            String classification, String accompanistName, int verification, string signedUp)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Classification = classification;
            this.AccompanistName = accompanistName;
            this.Verification = verification; 
        }

        public Student()
        {
            // TODO: Complete member initialization
        }

    }
}