﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JuryDuty.scheduler.entities
{
    public class Jury
    {
        private List<DateTime> schedule { get; set; }
        private int availableSlots { get; set; }
        private String classification { get; set; }
        private String roomNumber { get; set; }
        private int sessionLengthInMinutes { get; set; }
        private DateTime startTime { get; set; }
        private DateTime endTime { get; set; }

        public Jury(int scheduleSlots, String classification, String roomNumber,
            int sessionLengthInMinutes, DateTime startTime, DateTime endTime)
        {
            this.schedule = new List<DateTime>();
            this.availableSlots = scheduleSlots;
            this.classification = classification;
            this.roomNumber = roomNumber;
            this.sessionLengthInMinutes = sessionLengthInMinutes;
            this.startTime = startTime;
            this.endTime = endTime;
            renderScheduleSlots();
        }

        private void renderScheduleSlots()
        {
            int fillTimeHour = startTime.Hour;
            int fillTimeMinute = startTime.Minute;
            for (int i = 0; i < this.availableSlots; i++)
            {
                this.schedule.Add(new DateTime(startTime.Year, startTime.Month, startTime.Day, fillTimeHour, fillTimeMinute, 0));
                fillTimeMinute += this.sessionLengthInMinutes;
                //If minutes equals 60 increase hour and set minutes back to 0
                if(fillTimeMinute % 60 == 0)
                {
                    fillTimeMinute = 0;
                    fillTimeHour++;
                }
            }
        }
    }
}