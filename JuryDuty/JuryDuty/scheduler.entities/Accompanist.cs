﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JuryDuty.scheduler.entities
{
    public class Accompanist
    {
        private String firstName { get; set; }
        private String lastName { get; set; }
        
        public Accompanist(int scheduleSlots, String classification, String firstName,
            String lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}