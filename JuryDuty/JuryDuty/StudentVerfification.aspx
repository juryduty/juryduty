﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentVerfification.aspx.cs" Inherits="JuryDuty.StudentVerfification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Jury Duty</title>
    <link type="text/css" rel="stylesheet" href="~/css/studentView.css"/>
</head>
<body >
    <div class="layer" >
    <div id="outsideBorder">
    <h2>Welcome <span runat="server" id="fullName"></span>! <br /> Is the following information correct?</h2>
       <h4>Classification: <span runat="server" id="classification"></span></h4>
       <h4>Accompanist: <span runat="server" id="accompanist"></span></h4>
    <form id="form1" runat="server">
    <div class="confBtn">
         <asp:Button runat="server" ID="Yes" OnClick="yes_Click" text="Yes" Font-Size="Larger" />
         <asp:Button runat="server" ID="No" OnClick="no_Click"  text="No" Font-Size="Larger" />
    </div>
    </form>
    </div>
    </div>
</body>
</html>
